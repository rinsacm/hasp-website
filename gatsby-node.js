//

const path = require('path');

exports.createPages = ({ actions, graphql }) => {
    const { createPage } = actions;

    const achievementTemplate = path.resolve('src/templates/achievementTemplate.js');
    

    return graphql(
        `
            {
                allMarkdownRemark {
                    edges {
                        node {
                            id
                            frontmatter {
                                path
                            }
                        }
                    }
                }
            }
        `
    ).then(res => {
        if (res.errors) {
            return Promise.reject(errors);
        }

        res.data.allMarkdownRemark.edges.map(element => {
            let path = element.node.frontmatter.path;

            if (path) {
                //create page for blog using template
                if (path.includes(`/achievements/`)) {
                    createPage({
                        path,
                        component: achievementTemplate,
                    });
                } 
            }
        });
    });
};
