---
title: First Hacker Day
date: 27 Feb
cover: "./img1.jpg"
path: /achievements/hacker-day
desc: Hacker Day is an immersive hackathon where students with an interest in technology gather together to build a single project, a website for Hacker Space NSSCE. 
---
![Poster](./img1.jpg)


Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.