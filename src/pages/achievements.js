import React from 'react'
import Layout from '../components/layout'
import "../assets/css/achievements.scss"
import { Link,graphql } from "gatsby"
import Img from 'gatsby-image';
const achievements = ({data}) => (
  <Layout>
    <div className="container">
      <div>
          {data.allMarkdownRemark.edges.map(edge=>(
            <div className="Parent">
              <div className="box">
                <div class="circle">
                  <div class="circle__inner">
                    <div class="circle__wrapper">
                      <div class="circle__content">{edge.node.frontmatter.date}</div>
                    </div>
                  </div>
                </div>
                <div className="line"></div>
              </div>
              <div className="box1">
                <Link to={edge.node.frontmatter.path} class='link'>
                  <div className="box2">
                    <div  className='cover'>
                      <Img
                        style={{ height: '100%'}}
                                    fluid={
                                        edge.node.frontmatter.cover
                                            .childImageSharp.fluid
                                    }/>
                    </div>
                    <div className="ach-desc">
                      <h1 className="ach-desc-title">{edge.node.frontmatter.title}</h1>
                      <p>{edge.node.frontmatter.desc}</p>
 
                    </div>
                  </div>
                </Link>
              </div>
            </div>
       
         ))} 
          
         
        
      
        </div>
        </div>
      </Layout>
    
  )

  export const pageQuery=graphql`
  query achievements{
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
    ){
      edges{
        node{
          frontmatter{
            path
            cover{
              childImageSharp{
                fluid(maxWidth: 1000) {
                                    srcSet
                                    ...GatsbyImageSharpFluid_tracedSVG
                                }
              }
            }
          title
          date
          desc
          }
          
        }

      }
    }
  }
`;
console.log(pageQuery)
export default achievements
