import React from 'react';
import { graphql } from 'gatsby';
import SEO from '../components/seo';
import Layout from '../components/layout';
import '../assets/css/achievements_temp.scss'
export default function Template({
    data, // this prop will be injected by the GraphQL query below.
}) {
    const { markdownRemark } = data; // data.markdownRemark holds your post data
    const { frontmatter, html } = markdownRemark;
    return (
        <Layout>
            <SEO title={frontmatter.title}   />
            <div className='blog-box'>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <div >
               <div>
                   <h1 className='blog-title'>{frontmatter.title}</h1>
                    <div  class='blog-content' dangerouslySetInnerHTML={{ __html: html }}/>
                
                </div>
            
            </div>
            </div>
        </Layout>
        
    );
}

export const postQuery = graphql`
    query BlogPostByPath($path: String!) {
        markdownRemark(frontmatter: { path: { eq: $path } }) {
            html
            frontmatter {
                path
                title
                date
                
            }
            
        }
    }
`;
